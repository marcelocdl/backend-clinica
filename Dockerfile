FROM maven:3.8.1-openjdk-11-slim AS builder

# Define o diretório de trabalho no contêiner
WORKDIR /app

# Copia o arquivo pom.xml para o contêiner
COPY pom.xml .

# Baixa as dependências do Maven
RUN mvn dependency:go-offline

# Copia os arquivos de origem para o contêiner
COPY src ./src

# Compila o projeto Spring Boot
RUN mvn package

# Cria uma imagem menor para executar a aplicação
FROM openjdk:11-jre-slim

# Define o diretório de trabalho no contêiner
WORKDIR /app

# Copia o arquivo JAR da fase de compilação
COPY --from=builder /app/target/api_clinica-0.0.1-SNAPSHOT.jar /app/api_clinica-0.0.1-SNAPSHOT.jar

EXPOSE 8085

CMD ["java", "-jar", "api_clinica-0.0.1-SNAPSHOT.jar"]
